package com.example.slo_mo.restaurantlocator;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView cuisineListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        cuisineListView = (ListView) findViewById(R.id.list_view_cuisine);
        String[] values = getResources().getStringArray(R.array.string_array_cuisine);
        final ArrayList<String> listItems = new ArrayList<String>();
        for (int i = 0; i < values.length; ++i) {
            listItems.add(values[i]);
        }
        final ArrayAdapter adapter = new ArrayAdapter(this,R.layout.custom_textview, listItems);
        cuisineListView.setAdapter(adapter);

        cuisineListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, RestaurantsActivity.class);
                intent.putExtra("CUISINE", item.toLowerCase());
                startActivity(intent);
            }

        });
    }
}
