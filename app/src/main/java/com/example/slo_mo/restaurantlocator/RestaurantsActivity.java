package com.example.slo_mo.restaurantlocator;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class RestaurantsActivity extends AppCompatActivity {
    private String cuisine;
    private ListView restaurantListView;
    private ArrayList<Restaurant> restaurantList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurants);

        ActionBar ab = getSupportActionBar();
        ab.hide();

        cuisine = getIntent().getStringExtra("CUISINE");
        restaurantListView = (ListView) findViewById(R.id.list_view_restaurants);
        restaurantList = Restaurant.getRestaurantsFromFile("restaurants.json", this, cuisine);

        final ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> item;
        for (int i = 0; i < restaurantList.size(); i++) {
            Restaurant restaurant = restaurantList.get(i);
            item = new HashMap<String, String>();
            item.put("line1", restaurant.getName());
            item.put("line2", restaurant.getAddress());
            list.add(item);
        }

        SimpleAdapter simpleAdapter = new SimpleAdapter(this, list, R.layout.item_two_line,
                new String[]{"line1", "line2"}, new int[]{R.id.line_a, R.id.line_b});
        restaurantListView.setAdapter(simpleAdapter);

        restaurantListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(RestaurantsActivity.this, MapsActivity.class);
                intent.putExtra("RESTAURANT", restaurantList.get(position));
                startActivity(intent);
            }
        });
    }
}
