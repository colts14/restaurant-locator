package com.example.slo_mo.restaurantlocator;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by slo-mo on 12/12/2017.
 */

public class Restaurant implements Parcelable {

    private String cuisine;
    private String name;
    private String address;
    private String latitude;
    private String longitude;

    public String getCuisine() {
        return cuisine;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public Restaurant() {
    }

    public static ArrayList<Restaurant> getRestaurantsFromFile(String filename, Context context, String cuisineFilter) {
        final ArrayList<Restaurant> restaurantList = new ArrayList<>();

        try {
            // Load data
            String jsonString = loadJsonFromAsset("restaurants.json", context);
            JSONObject json = new JSONObject(jsonString);
            JSONArray restaurants = json.getJSONArray("restaurants");

            // Get Restaurant objects from data
            for (int i = 0; i < restaurants.length(); i++) {
                Restaurant restaurant = new Restaurant();

                restaurant.cuisine = restaurants.getJSONObject(i).getString("cuisine");
                restaurant.name = restaurants.getJSONObject(i).getString("name");
                restaurant.address = restaurants.getJSONObject(i).getString("address");
                restaurant.latitude = restaurants.getJSONObject(i).getString("latitude");
                restaurant.longitude = restaurants.getJSONObject(i).getString("longitude");

                if (restaurant.cuisine.equals(cuisineFilter.toLowerCase())) {
                    restaurantList.add(restaurant);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return restaurantList;
    }

    //---- Adapted from https://stackoverflow.com/questions/41527906/getassets-from-another-class-new-post-see-loadjsonfromasset ----//
    private static String loadJsonFromAsset(String filename, Context context) {
        String json = null;

        try {
            InputStream is = context.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (java.io.IOException ex) {
            ex.printStackTrace();
            return null;
        }

        return json;
    }


    protected Restaurant(Parcel in) {
        cuisine = in.readString();
        name = in.readString();
        address = in.readString();
        latitude = in.readString();
        longitude = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cuisine);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(latitude);
        dest.writeString(longitude);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Restaurant> CREATOR = new Parcelable.Creator<Restaurant>() {
        @Override
        public Restaurant createFromParcel(Parcel in) {
            return new Restaurant(in);
        }

        @Override
        public Restaurant[] newArray(int size) {
            return new Restaurant[size];
        }
    };
}